class Checkout

  def initialize(rules)
    @rules = rules
    @all_products = []
  end

  def scan(product)
    @all_products << product
  end

  def total
    @sum_total = 0
    check_special_price if @all_products.count > 0
    @sum_total
  end

  private
  def check_special_price
    @rules.each do |special_price_rule, v|
      @count = 0
      @product = @rules[special_price_rule]['product_name']
      if @all_products.include?(@product)
        @count = @all_products.count(@product)
        apply_special_price(@count, @rules[special_price_rule])
      end
    end
  end

  def apply_special_price(count, special_price_rule)
    @special_quantity = special_price_rule['how_many'] if special_price_rule.has_key? 'how_many'
    @product = special_price_rule['product_name']
    @special_price = special_price_rule['price']

    if (special_price_rule.has_key?('how_many') && count >= @special_quantity)
      quocient = count % @special_quantity
      part_of_sum = count / @special_quantity
      @sum_special = quocient * special_price_rule['unit']
      @sum_unit = part_of_sum * @special_price
      @sum = @sum_unit + @sum_special
    else
      @sum = count * special_price_rule['unit']
    end
    @sum_total = @sum_total + @sum
  end
end
