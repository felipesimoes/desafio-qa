# language: pt
Funcionalidade: Criar um grupo

  Cenario: Criar um grupo de conversa
    Dado que eu esteja na tela de conversas
    Quando eu crio um novo grupo
    E adiciono participantes
    Então eu posso visualizar esse grupo
