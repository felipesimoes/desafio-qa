# language: pt
Funcionalidade: Adicionar um atalho de conversa

  Cenario: Adicionar um atalho de conversa
    Dado que eu esteja na tela de conversas
    Quando eu marco uma conversa
    E seleciono Adicionar atalho para conversa
    Então eu posso acessar a conversa pelo atalho
